//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------
cbuffer cbPerObject : register( b0 )
{
	float4	g_vObjectColor    : packoffset(c0);
  float 	g_vIOR            : packoffset(c1.r); // Index of Refraction
  float   g_vRoughness      : packoffset(c1.g);
  float   g_vMetalness      : packoffset(c1.b);
};

cbuffer cbPerFrame : register( b1 )
{
	float3	g_vLightDir				: packoffset( c0 );
};

//--------------------------------------------------------------------------------------
// Textures and Samplers
//--------------------------------------------------------------------------------------
TextureCube	g_txEnvironment : register( t0 );
TextureCube	g_txIrradiance : register(t1);
SamplerState g_samLinear : register( s0 );

//--------------------------------------------------------------------------------------
// Input / Output structures
//--------------------------------------------------------------------------------------
struct PS_INPUT
{
	float3 vNormal		: NORMAL;
  float3 pos        : POSITION;
  float3 worldReflection : TEXCOORD;
};

float4 Specular(PS_INPUT Input, float NdotL, float4 lightColour, float3 lightDir, out float3 fresnel)
{
  float4 specular = 0.0;
  float3 eyeDir = -normalize(Input.pos).xyz;

  //calculate intermediary values
  float3 halfVector = normalize(lightDir + eyeDir);
  float NdotH = dot(Input.vNormal, halfVector);
  float NdotV = dot(Input.vNormal, eyeDir);
  float VdotH = dot(eyeDir, halfVector);
  float mSquared = g_vRoughness * g_vRoughness;

  //  geometric attenuation
  float geoAtt = 4 * VdotH * VdotH;

  //  roughness
  //beckmann distribution function
  float r1 = 1.0 / (mSquared * pow(NdotH, 4.0));
  float r2 = (NdotH * NdotH - 1.0) / (mSquared * NdotH * NdotH);
  float roughness = r1 * exp(r2);

  //  fresnel
  //  Schlick approximation
  float3 F0 = (1.0 - g_vIOR) / (1.0 + g_vIOR);
  F0 *= F0;
  F0 = lerp(F0, g_vObjectColor.rgb, g_vMetalness);

  fresnel = pow(1.0 - VdotH, 5.0);
  fresnel *= (1.0 - F0);
  fresnel += F0;

  specular = (fresnel.xyzz / geoAtt * roughness) / (NdotV * 3.14159265);
  return NdotL * specular * lightColour;
}

//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PSMain( PS_INPUT Input ) : SV_TARGET
{
  float4 lightColour = 1.0;

  float4 diffuse = 0.0;
  float4 specular = 0.0;
  float3 fresnel = 0.0;

  // Environment lighting
  float NdotL = saturate(dot(Input.worldReflection, Input.vNormal));
  if (NdotL > 0.0)
    specular += Specular(Input, NdotL, g_txEnvironment.Sample(g_samLinear, Input.vNormal), Input.worldReflection, fresnel);
  diffuse += g_txIrradiance.Sample(g_samLinear, Input.vNormal) * (1 - fresnel.xyzz) * (1 - g_vMetalness) * g_vObjectColor;

  // Scene lighting
  NdotL = saturate(dot(g_vLightDir, Input.vNormal));
  if (NdotL > 0.0)
    specular += Specular(Input, NdotL, lightColour, g_vLightDir, fresnel);
  diffuse += NdotL * lightColour * (1 - fresnel.xyzz) * (1 - g_vMetalness) * g_vObjectColor;

	return diffuse + specular;
}

