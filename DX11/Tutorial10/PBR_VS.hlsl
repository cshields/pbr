//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------
cbuffer cbPerObject : register( b0 )
{
	matrix		g_mWorldViewProjection	: packoffset( c0 );
	matrix		g_mNormal				: packoffset( c4 );
  matrix		g_mModelView		: packoffset(c8);
  float4    g_cameraPos     : packoffset(c12);
};

//--------------------------------------------------------------------------------------
// Input / Output structures
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
	float4 vPosition	: POSITION;
	float3 vNormal		: NORMAL;
};

struct VS_OUTPUT
{
	float3 vNormal		: NORMAL;
  float3 pos        : POSITION;
  float3 worldReflection : TEXCOORD;
  float4 vPosition	: SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain( VS_INPUT Input )
{
	VS_OUTPUT Output;
	
  Output.pos = mul(Input.vPosition - g_cameraPos, g_mNormal).xyz;
	Output.vNormal = mul( Input.vNormal, (float3x3)g_mNormal);
  Output.worldReflection = reflect(normalize(Output.pos), Output.vNormal);

  Output.vPosition = mul(Input.vPosition, g_mWorldViewProjection);
	
	return Output;
}

