//--------------------------------------------------------------------------------------
// Textures and Samplers
//--------------------------------------------------------------------------------------
TextureCube	 g_txDiffuse : register(t0);

SamplerState g_samLinear  : register(s0);


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PSMain(float3 vTexCoord	: TEXCOORD) : SV_TARGET
{
  return g_txDiffuse.Sample(g_samLinear, vTexCoord);
}