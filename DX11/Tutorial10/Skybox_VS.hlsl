
//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------
cbuffer cbPerObject : register(b0)
{
  matrix		g_mProjection	: packoffset(c0);
  matrix		g_mView			: packoffset(c4);
};

//--------------------------------------------------------------------------------------
// Input / Output structures
//--------------------------------------------------------------------------------------
struct VS_INPUT
{
  float4 vPosition	: POSITION;
};

struct VS_OUTPUT
{
  float3 vTexCoord	: TEXCOORD;
  float4 vPosition	: SV_POSITION;
};

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
VS_OUTPUT VSMain(VS_INPUT Input)
{
  VS_OUTPUT Output;

  Output.vPosition = float4(mul(Input.vPosition, (float3x3)g_mView),  1.0);
  Output.vTexCoord = normalize(Input.vPosition.xyz);

  Output.vPosition = mul(Output.vPosition, g_mProjection);

  return Output;
}

